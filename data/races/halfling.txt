The comforts of home are the goals of most halflings' lives: a place to  settle in peace and quiet, far from marauding monsters and clashing armies; a blazing fire and a generous meal; fine drink and fine conversation. Though some halflings live out their days in remote agricultural communities, others form nomadic bands that travel constantly, lured by the open road and the wide horizon to discover the wonders of new lands and peoples. But even these wanderers love peace, food, hearth, and home, though home might be a wagon jostling along an dirt road or a raft floating downriver.

Ability Score Increase: Your Dexterity score increases by 2.

Age: A halfling reaches adulthood at the age of 20 and generally lives into the middle of his or her second century.

Alignment: Most halflings are lawful good. As a rule, they are good-hearted and kind, hate to see others in pain, and have no tolerance for oppression. They are also very orderly and traditional, leaning heavily on the support of their community and the comfort of their old ways.

Size: Halflings average about 3 feet tall and weigh about 40 pounds. Your size is Small.

Speed: Your base walking speed is 25 feet.

Lucky: When you roll a 1 on an attack roll, ability check, or saving throw, you can reroll the die and must use the new roll.

Brave: You have advantage on saving throws against being frightened.

Halfling Nimbleness: You can move through the space of any creature that is of a size larger than yours.

Languages: You can speak, read, and write Common and Halfling. The Halfling language isn’t secret, but halflings are loath to share it with others. They write very little, so they don’t have a rich body of literature. Their oral tradition, however, is very strong. Almost all halflings speak Common to converse with the people in whose lands they dwell or through which they are traveling.

Subrace: The two main kinds of halfling, lightfoot and stout, are more like closely related families than true subraces.
