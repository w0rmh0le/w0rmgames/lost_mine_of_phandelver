use std::io;

// #[derive(Copy, Clone)]
enum PlayableRace {
    Dwarf(Option<Subrace>),
    Elf(Option<Subrace>),
    Halfling(Option<Subrace>),
    Human(Option<Subrace>),
    Dragonborn(Option<Subrace>),
    Gnome(Option<Subrace>),
    HalfElf(Option<Subrace>),
    HalfOrc(Option<Subrace>),
    Tiefling(Option<Subrace>)
}

impl PlayableRace {
    fn get_proper_noun(&self) -> &str {
        match self {
            Race::Dwarf => "Dwarf",
            Race::Elf => "Elf",
            Race::Halfling => "Halfling",
            Race::Human => "Human",
            Race::Dragonborn => "Dragonborn",
            Race::Gnome => "Gnome",
            Race::HalfElf => "Half-Elf",
            Race::HalfOrc => "Half-Orc",
            Race::Tiefling => "Tiefling"
        }
    }
}

// #[derive(Copy, Clone)]
enum Subrace {
    MountainDwarf,
    HillDwarf,
    HighElf,
    WoodElf,
    DarkElf,
    LightfootHalfling,
    StoutHalfling,
    ForestGnome,
    RockGnome
}

// #[derive(Copy, Clone)]
enum Class {
    Barbarian,
    Bard,
    Cleric,
    Druid,
    Fighter,
    Monk,
    Paladin,
    Ranger,
    Rogue,
    Sorcerer,
    Warlock,
    Wizard
}

struct Character {
    name: &str,
    race: Race(Option<Subrace>),
    class: Class
}

fn main() {

    // This data should be imported from where "PlayableRace" is defined.
    const playable_races = [
        "Dwarf",
        "Elf",
        "Halfling",
        "Human",
        "Dragonborn",
        "Gnome",
        "Half-Elf",
        "Half-Orc",
        "Tiefling"
    ];

    let name = {
        print_dialogue("System", "Please enter your character's name.");
        &(get_sentence().join(" ")[..]) // .join() returns a String. &(String[..]) retrieves a &str
    };

    let race = {
        loop {
            print_dialogue("System", "Please select a race.");
            for race in &races {
                println!("> {}", race);
            }
            let sentence: Vec<String> = get_sentence();

            if sentence[0]("dwarf") {
                Race::Dwarf(Race::get_subrace())
            }
            else if race.contains("elf") {
                Race::Elf(None)
            }
            else if race.contains("halfling") {
                Race::Halfling(None)
            }
            else if race.contains("human") {
                Race::Human(None)
            }
            else if race.contains("dragonborn") {
                Race::Dragonborn(None)
            }
            else if race.contains("gnome") {
                Race::Gnome(None)
            }
            else if race.contains("half") && race.contains("elf") {
                Race::HalfElf(None)
            }
            else if race.contains("half") && race.contains("orc") {
                Race::HalfOrc(None)
            }
            else if race.contains("tiefling") {
                Race::Tiefling(None)
            }
            else {
                print_dialogue("System",
                    "Unable to determine race selection: Defaulting to human...");
                Race::Human(None)
            };

            if selection_confirmed(get_race_proper_noun(race)) {
                break race
            }
        }
    };

    let class = {

        let classes = [
            "Barbarian",
            "Bard",
            "Cleric",
            "Druid",
            "Fighter",
            "Monk",
            "Paladin",
            "Ranger",
            "Rogue",
            "Sorcerer",
            "Warlock",
            "Wizard"
        ];

        loop {
            print_dialogue("System",
                "Please choose a class.");
        
            for class in &classes {
                println!("> {}", class);
            }

            let class = get_input().to_ascii_lowercase();
            
            let class = if class.contains("barbarian") {
                Class::Barbarian
            }
            else if class.contains("bard") {
                Class::Bard
            }
            else if class.contains("cleric") {
                Class::Cleric
            }
            else if class.contains("druid") {
                Class::Druid
            }
            else if class.contains("fighter") {
                Class::Fighter
            }
            else if class.contains("monk") {
                Class::Monk
            }
            else if class.contains("paladin") {
                Class::Paladin
            }
            else if class.contains("ranger") {
                Class::Ranger
            }
            else if class.contains("rogue") {
                Class::Rogue
            }
            else if class.contains("sorcerer") {
                Class::Sorcerer
            }
            else if class.contains("warlock") {
                Class::Warlock
            }
            else if class.contains("wizard") {
                Class::Wizard
            }
            else {
                print_dialogue("System",
                    "Unable to determine class selection: Defaulting to fighter...");
                Class::Fighter
            };

            if selection_confirmed(get_class_proper_noun(class)) {
                break class
            }
        }
    };

    // Determine Ability Scores

    // Describe Your Character

    // Choose Equipment

    let player = Character {
        name,
        race,
        subrace,
        class
    };

    println!("Greetings {}, {} of the {}!",
        player.name,
        get_class_proper_noun(player.class),
        if player.subrace.is_some() {
            get_subrace_proper_noun_plural(player.subrace)
        }
        else {
            get_race_proper_noun(player.race)
        }
    );
}

// ------------------------------------------------------------------------------------------------
// Output
// ------------------------------------------------------------------------------------------------

fn print_dialogue(speaker: &str, sentence: &str) {
    println!("{}: {}", speaker, sentence);
}

// ------------------------------------------------------------------------------------------------
// Input
// --------------------------------------------------------------------------------------------

/// Retrieves a line of terminal input from the user, trimming leading and trailing whitespace.
/// 
/// ### Example
/// ```
/// let user_input = get_input();
/// // Terminal input: " Hello, world!\n"
/// assert_eq!(user_input, "Hello, world");
/// ```
fn get_input() -> &str {
    let mut buffer = String::new();

    io::stdin().read_line(&mut buffer)
        .expect("An error was encountered while reading user input: Terminating application...");

    buffer.trim()
}

/// Retrieves a line of terminal input from the user, separating words on whitespace.
/// 
/// ### Example
/// ```
/// let sentence: Vec<&str> = get_sentence();
/// // Terminal input: "My name is Alex."
/// assert_eq!(sentence, ["My", "name", "is", "Alex."]);
/// ```
fn get_sentence() -> Vec<String> {
    let input = loop {
        let input = get_input();
        if !input.is_empty() {
            break input
        }
    }

    let bytes = input.as_bytes();
    let word: Vec<u8> = Vec::new();
    let sentence: Vec<&str> = Vec::new();

    for (byte) in &bytes {
        if byte != b' ' {
            word.push(byte);
        }
        else {
            if word.len() != 0 {
                sentence.push(&word.iter().collect()[..]);
                word.clear();
            }
        }
    }
    sentence
}

fn selection_confirmed(selection: Vec<String>) -> bool {
    println!("System: Confirm selection (y/n): {}", selection);
    let confirmation: Vec<String> = get_sentence();
    if confirmation[0].eq_ignore_ascii_case("y") {
        true
    }
    else {
        false
    }
}

// ------------------------------------------------------------------------------------------------
// Other
// ------------------------------------------------------------------------------------------------

impl Race {
    fn get_subrace() -> Subrace {

        let subraces = match race {
            Race::Dragonborn => [],
            Race::Dwarf => [ "Mountain Dwarf", "Hill Dwarf" ],
            Race::Elf => [ "High Elf", "Wood Elf", "Dark Elf" ],
            Race::Gnome => [ "Forest Gnome", "Rock Gnome" ],
            Race::HalfElf => [],
            Race::Halfling => [ "Lightfoot Halfling", "Stout Halfling" ],
            Race::HalfOrc => [],
            Race::Human => [],
            Race::Tiefling => []
        }
    
        if subraces.len() == 0 {
            None
        }
    
        print_dialogue("System",
            "Please choose a subrace.");
    
        match race {
            Race::Dwarf => {
    
                for subrace in &subraces {
                    println!("> {}", subrace);
                }
    
                let subrace = get_input().to_ascii_lowercase();
    
                let subrace = if subrace.contains("mountain") {
                    Some(Subrace::MountainDwarf)
                }
                else if subrace.contains("hill") {
                    Some(Subrace::HillDwarf)
                }
                else {
                    println!("System: Unable to determine dwarven subrace selection: Defaulting to mountain dwarf...");
                    Some(Subrace::MountainDwarf)
                };
    
                if selection_confirmed(get_subrace_proper_noun(subrace.unwrap())) {
                    subrace
                }
            },
            Race::Elf => {
    
                for subrace in &subraces {
                    println!("> {}", subrace);
                }
    
                let subrace = get_input().to_ascii_lowercase();
    
                let subrace = if subrace.contains("high") {
                    Some(Subrace::HighElf)
                }
                else if subrace.contains("wood") {
                    Some(Subrace::WoodElf)
                }
                else if subrace.contains("dark") {
                    Some(Subrace::DarkElf)
                }
                else {
                    println!("System: Unable to determine elven subrace selection: Defaulting to high elf...");
                    Some(Subrace::HighElf)
                };
    
                if selection_confirmed(get_subrace_proper_noun(subrace)) {
                    break subrace
                }
            },
            Race::Halfling => {
    
                for subrace in &subraces {
                    println!("> {}", subrace);
                }
    
                let subrace = get_input().to_ascii_lowercase();
    
                let subrace = if subrace.contains("lightfoot") {
                    Some(Subrace::LightfootHalfling)
                }
                else if subrace.contains("stout") {
                    Some(Subrace::StoutHalfling)
                }
                else {
                    println!("System: Unable to determine halfling subrace selection: Defaulting to lightfoot halfling...");
                    Some(Subrace::LightfootHalfling)
                };
    
                if selection_confirmed(get_subrace_proper_noun(subrace)) {
                    break subrace
                }
            },
            Race::Human => break None,
            Race::Dragonborn => break None,
            Race::Gnome => {
    
                for subrace in &subraces {
                    println!("> {}", subrace);
                }
    
                let subrace = get_input().to_ascii_lowercase();
    
                let subrace = if subrace.contains("forest") {
                    Some(Subrace::ForestGnome)
                }
                else if subrace.contains("rock") {
                    Some(Subrace::RockGnome)
                }
                else {
                    println!("System: Unable to determine gnomish subrace selection: Defaulting to forest gnome...");
                    Some(Subrace::MountainDwarf)
                };
    
                if selection_confirmed(get_subrace_proper_noun(subrace)) {
                    break subrace
                }
            },
            Race::HalfElf => break None,
            Race::HalfOrc => break None,
            Race::Tiefling => break None
        }
    }
}

fn get_subrace_proper_noun(subrace: Option<Subrace>) -> String {
    match subrace {
        Some(Subrace::MountainDwarf) => String::from("Mountain Dwarf"),
        Some(Subrace::HillDwarf) => String::from("Hill Dwarf"),
        Some(Subrace::HighElf) => String::from("High Elf"),
        Some(Subrace::WoodElf) => String::from("Wood Elf"),
        Some(Subrace::DarkElf) => String::from("Dark Elf"),
        Some(Subrace::LightfootHalfling) => String::from("Lightfoot Halfling"),
        Some(Subrace::StoutHalfling) => String::from("Stout Halfling"),
        Some(Subrace::ForestGnome) => String::from("Forest Gnome"),
        Some(Subrace::RockGnome) => String::from("Rock Gnome"),
        None => String::from("An error was encountered while attempting to retrieve this character's subrace: No subrace found.")
    }
}

fn get_subrace_proper_noun_plural(subrace: Option<Subrace>) -> String {
    match subrace {
        Some(Subrace::MountainDwarf) => String::from("Mountain Dwarves"),
        Some(Subrace::HillDwarf) => String::from("Hill Dwarves"),
        Some(Subrace::HighElf) => String::from("High Elves"),
        Some(Subrace::WoodElf) => String::from("Wood Elves"),
        Some(Subrace::DarkElf) => String::from("Dark Elves"),
        Some(Subrace::LightfootHalfling) => String::from("Lightfoot Halflings"),
        Some(Subrace::StoutHalfling) => String::from("Stout Halflings"),
        Some(Subrace::ForestGnome) => String::from("Forest Gnomes"),
        Some(Subrace::RockGnome) => String::from("Rock Gnomes"),
        None => String::from("An error was encountered while attempting to retrieve this character's subrace: No subrace found.")
    }
}

fn get_class_proper_noun(class: Class) -> String {
    match class {
        Class::Barbarian => String::from("Barbarian"),
        Class::Bard => String::from("Bard"),
        Class::Cleric => String::from("Cleric"),
        Class::Druid => String::from("Druid"),
        Class::Fighter => String::from("Fighter"),
        Class::Monk => String::from("Monk"),
        Class::Paladin => String::from("Paladin"),
        Class::Ranger => String::from("Ranger"),
        Class::Rogue => String::from("Rogue"),
        Class::Sorcerer => String::from("Sorcerer"),
        Class::Warlock => String::from("Warlock"),
        Class::Wizard => String::from("Wizard")
    }
}

// Unused but potentially useful

// fn get_race_proper_noun_plural(race: Race) -> String {
//     match race {
//         Race::Dwarf => String::from("Dwarves"),
//         Race::Elf => String::from("Elves"),
//         Race::Halfling => String::from("Halflings"),
//         Race::Human => String::from("Humans"),
//         Race::Dragonborn => String::from("Dragonborn"),
//         Race::Gnome => String::from("Gnomes"),
//         Race::HalfElf => String::from("Half-Elves"),
//         Race::HalfOrc => String::from("Half-Orcs"),
//         Race::Tiefling => String::from("Tieflings")
//     }
// }

// fn get_race_noun_plural(race: Race) -> String {
//     match race {
//         Race::Dwarf => String::from("dwarves"),
//         Race::Elf => String::from("elves"),
//         Race::Halfling => String::from("halflings"),
//         Race::Human => String::from("humans"),
//         Race::Dragonborn => String::from("dragonborn"),
//         Race::Gnome => String::from("gnomes"),
//         Race::HalfElf => String::from("half-elves"),
//         Race::HalfOrc => String::from("half-orcs"),
//         Race::Tiefling => String::from("tieflings")
//     }
// }